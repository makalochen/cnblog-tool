# cnblog-tool

QQ交流群：570182306

由于博客的MetaWeblog Api	更新比较频繁  此仓库只修复问题，不再开发新的功能



## 介绍
cnblog-tool 是用node electron 开发的一个博客园发布博客的小工具

## 软件架构
使用node+electron+html+css+js

## cnblog-tool能干啥

- 解析md文件中的本地图片上传到博客园的图床
- 上传md文件内容到指定分类
- 将md中的文件图片全部转换成base64格式，不依赖任何图床
- 下载博客中部分公开帖子为markdown 格式(V4.0)



## 安装教程

### 下载

win：[v4.0](https://pan.baidu.com/s/1_utUZweOEpEuszPhZqPlaQ?pwd=rr7a ) 提取码：rr7a

[v3.0](https://gitee.com/makalochen/cnblog-tool/blob/master/exe/makalo-cnblog-tool%20Setup%203.0.0.exe)

mac 版：请加群获取

### 安装

双击下载下来的exe安装程序

## 使用说明



初次使用，会让你登录，该账号是你博客园的用户名和[MetaWeblog访问令牌](https://i.cnblogs.com/settings#enableServiceAccess)，之所以要登录，是因为该工具基于博客园的`metaweblog`，登录成功会在你的安装目录下有个`cnblog-tool.json`的配置文件，里面保存了你的配置，如果需要重置直接删除此文件即可



### 初次使用

![image-20220621175911809](assets/image-20220621175911809.png)

登录成功会写入配置

### 本地图片上传到博客园的图床

找md文件，选中后鼠标右击，如

![image-20210305164400350](assets/image-20210305164400350.png)

上传完了会在同目录下生成一个已经替换好的文件，如

![image-20210305164534005](assets/image-20210305164534005.png)

![image-20210305164554849](assets/image-20210305164554849.png)

### 上传md文件内容到指定分类

选中生成好的文件，鼠标右击

![image-20210305164728997](assets/image-20210305164728997.png)

选择分类，点击确认上传

![image-20210305164826858](assets/image-20210305164826858.png)

上传完了后，会返回博客园的链接，可直接复制到浏览器查看

![image-20210305164921934](assets/image-20210305164921934.png)

### markdwon中的图片转换成base64

右击md文件，选择转换

![image-20220530105851403](assets/image-20220530105851403.png)

会生成一个新的文件

![image-20220530105935161](assets/image-20220530105935161.png)

文件打开会发现，文件内容中的图片已经转换成base64格式了

## 关于mac版特别说明

### 关于Mac 版实现方案问题

因为mac 要配置info.plist,才有对应的右键菜单， 相对应地electron-build 我不知道怎么配置，询问了多个大佬，想出了一个折中的方案 ：

在win版上我是直接添加了几个右键菜单，唤起我的应用，并且打开不同的页面，mac也可以，因为我看到wps 也有添加右键菜单（我太菜了），配置相对复杂

想出了一个折中的，那就是添加文件在mac 中添加文件唤醒，但是文件唤醒只有唤起文件的路径，并且不能自定义参数，所以我就是根据文件名去判断最为核心的两步

- 将markdown 文件中的图片上传到图床
- 将生成的文件同步到博客园

上面这两步是根据文件名中有没有 `-cnblog`这一串字符来区分的

### 关于会修改的md 文件默认打开方式的问题

单击选中一个以`.md`结尾的文件，`command + i`	快速打开文件简介，如图所示更改回来即可

![image-20230222153046809](assets/image-20230222153046809.png)

## 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request