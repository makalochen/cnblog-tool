const { app } = require('electron');
const bw = require('./mainLib/browserWindow');
const util = require('./mainLib/util');
const log = require('./lib/log');

// 文件路径
let filePath = '';
// 因为mac 比较特殊，所以使用这种方法投机取巧一下
app.on('open-file', (event, path) => {
  event.preventDefault();
  filePath = path;
  log.setLog(`触发open-file事件, 文件路径: ${path}\n`, util.logPath);
});

// 应用启动时检查配置文件是否存在
app.on('ready', () => {
  // 配置文件不存在
  if (!util.isLogin()) {
    bw.showLoginWindow();
    return;
  }
  log.setLog(`触发ready事件, 文件路径: ${filePath}\n`, util.logPath);
  // mac 环境
  if (filePath) {
    // 判断文件名结尾，根据文件结尾判断调用那个渲染页面
    if (filePath.indexOf('-cnblog') !== -1) {
      // 上传到博客园
      bw.switchWindow('upload', filePath);
    } else {
      // 上传图片
      bw.switchWindow('read', filePath);
    }
  } else {
    // win 环境
    // 参数数组 第一个参数为程序运行路径 后面都是以空格分隔的指令
    const { argv } = process;
    // argv[1] 是注册表中的指令
    // argv[2] 就是文件或文件夹路径路径
    bw.switchWindow(argv[1], argv[2]);
  }
});

