const { BrowserWindow } = require('electron');
const path = require('path');
const config = require('../lib/config');
const util = require('../lib/util');
const md = require('../lib/md');
/**
 * 注意:此类只能在主进程中使用
 */


module.exports = {
  // 显示登录窗口
  showLoginWindow() {
    const win = new BrowserWindow({
      width: 400,
      height: 400,
      // frame: false,
      webPreferences: {
        // 是否注入nodeapi
        nodeIntegration: true,
        // 渲染进程是否启用remote模块
        enableRemoteModule: true,
      },
    });
    win.loadFile('src/html/login.html');
  },

  // 显示上传图片窗口
  showUploadImageWindow(filePath) {
    const win = new BrowserWindow({
      width: 600,
      height: 800,
      // frame: false,
      webPreferences: {
        // 是否注入nodeapi
        nodeIntegration: true,
        // 渲染进程是否启用remote模块
        enableRemoteModule: true,
      },
    });
    win.loadFile('src/html/uploadImage.html');

    // 发送消息
    win.webContents.on('did-finish-load', () => {
      win.webContents.send('file-path-event', filePath);
    });
  },

  // 显示上传博客园窗口
  showUploadCNBlogWindow(filePath) {
    const win = new BrowserWindow({
      width: 600,
      height: 800,
      // frame: false,
      webPreferences: {
        // 是否注入nodeapi
        nodeIntegration: true,
        // 渲染进程是否启用remote模块
        enableRemoteModule: true,
      },
    });
    win.loadFile('src/html/uploadCNBlog.html');

    // 发送消息
    win.webContents.on('did-finish-load', () => {
      win.webContents.send('file-path-event', filePath);
    });
  },
  // 显示下载窗口
  showDownCNBlogdWindow(filePath, userInfo) {
    const win = new BrowserWindow({
      width: 1200,
      height: 800,
      // frame: false,
      webPreferences: {
        // 是否注入nodeapi
        nodeIntegration: true,
        // 渲染进程是否启用remote模块
        enableRemoteModule: true,
      },
    });
    win.loadFile('src/html/downCNBlog.html');


    // 发送消息
    win.webContents.on('did-finish-load', () => {
      const descdir = path.join(util.replaceFileName(filePath), userInfo.username);
      win.webContents.send('file-path-event', { descdir, userInfo, config });
    });
  },
  // 根据指令显示窗口
  switchWindow(command, filePath) {
    // argv[1] 是注册表中的指令
    // argv[2] 就是文件或文件夹路径路径
    switch (command) {
      case 'read':
        this.showUploadImageWindow(filePath);
        break;
      case 'upload':
        this.showUploadCNBlogWindow(filePath);
        break;
      case 'conver-base64':
        md.getMdByBase64(filePath);
        this.quit();
        break;
      case 'down':
        this.showDownCNBlogdWindow(filePath, util.getUserInfo());
        break;
      default:
        break;
    }
  },
};
