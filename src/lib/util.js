class Util {
  /**
   * 替换不规则的文件名或文文件路径  \ / : * ? " < > |
   * @param {string} filename 文件名
   * @returns 符合规范的文件名
   */
  replaceFileName(filename, replaceStr = '') {
    // const pattern = /[\\\\/:*?\"<>|]/g;

    // 取消盘符的匹配
    // eslint-disable-next-line no-useless-escape
    const pattern = /[\"]/g;
    // 返回匹配结构并只保留本地图片 和 本地文件存在的
    return filename.replace(pattern, replaceStr);
  }
}
module.exports = new Util();
