const got = require('got');
const cheerio = require('cheerio');

/**
 * 此类用于检测cnblogs编辑器是否是markdown类型
 */
class IsMd {
  /**
   * 构造函数
   * @param {string} url 要检测的链接
   * @param {Function} callback 回调函数
   */
  constructor(url = '', callback) {
    (async (link) => {
      try {
        const response = await got(link, {
          headers: {
            'Accept-Encoding': '',
            'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/102.0.5005.63 Safari/537.36',
          },
          https: {
            // 不检测ssl
            rejectUnauthorized: false,
          },
        });
        const $ = cheerio.load(response.body);
        const isMd = $('#cnblogs_post_body').hasClass('cnblogs-markdown');
        callback(isMd);
      } catch (error) {
        console.log('error:', error);
      }
    })(this.autoURL(url));
  }

  autoURL(url = '') {
    let autoUrl = url;
    if (url.substring(0, 4) !== 'http') {
      autoUrl = `https:${url}`;
    }
    return autoUrl;
  }
}
module.exports = IsMd;
