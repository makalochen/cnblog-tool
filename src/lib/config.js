// 存放全局固定配置
module.exports = {
  // app自动退出时间 单位毫秒
  appAutoExitTime: 3000,
  // 默认下载博客帖子数量
  downloadCount: 50,
  // 下载博文是否下载图片
  downImageFlag: false,
};
