// 消息中心
class MsgCenter {
  // 全局共享消息对象
  static msgObj = {};

  // 订阅
  on(msgName, func) {
    MsgCenter.msgObj[msgName] = func;
  }

  // 发布
  emit(msgName, data) {
    if (!Object.prototype.hasOwnProperty.call(MsgCenter.msgObj, msgName)) return;
    MsgCenter.msgObj[msgName](data);
  }

  // 取消订阅
  off(msgName) {
    if (!Object.prototype.hasOwnProperty.call(MsgCenter.msgObj, msgName)) return;
    delete MsgCenter.msgObj[msgName];
  }
}

module.exports = new MsgCenter();
