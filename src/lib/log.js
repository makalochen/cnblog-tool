const fs = require('fs');

module.exports = {
  // 写日志文件
  setLog(message, path) {
    fs.writeFileSync(path, message, { flag: 'a' });
  },
};
