const got = require('got');
const fs = require('fs');
const path = require('path');
const html2md = require('html-to-md');
const md = require('./md');
const rpc = require('./rpc');
const mc = require('./MsgCenter');
const config = require('./config');


class DownloadMd {
  // 下载的目标文件夹路径
  static descFolder = '';

  // 构造函数
  constructor(descFolder = '') {
    this.descFolder = path.resolve(descFolder);
    // 文件夹路径不存在则创建
    fs.existsSync(descFolder) === false && this.mkdirs(descFolder);
    // 发布消息
    mc.emit('DownloadMd.constructor', `${descFolder}`);
  }

  // 递归创建文件夹
  mkdirs(dirpath) {
    if (!fs.existsSync(path.dirname(dirpath))) {
      this.mkdirs(path.dirname(dirpath));
    }
    fs.mkdirSync(dirpath);
  }

  /**
   * 替换不规则的文件名  \ / : * ? " < > |
   * @param {string} filename 文件名
   * @returns 符合规范的文件名
   */
  replaceFileName(filename) {
    // eslint-disable-next-line no-useless-escape
    const pattern = /[\\\\/:*?\"<>|]/g;
    // 返回匹配结构并只保留本地图片 和 本地文件存在的
    return filename.replace(pattern, ' ');
  }

  /**
   * 将博客园的帖子下载到本地
   * @param {*} postObj 帖子对象信息
   * @param {*} isMd 是否是md类型
   * @param {*} abstractPath 要下载的文件夹路径，不指定则是构造函数创建时指定
   */
  postTolocal(postObj = {}, isMd = true, abstractPath = this.descFolder) {
    // 发布消息
    mc.emit('DownloadMd.postTolocal.start', { title: postObj.title, postId: postObj.postid });

    // 将文件内容写入文件, 文件名：博客标题 + 博客id + md,不存在则创建
    const categories = rpc.getPostCategory(postObj.categories);

    // 查询是否是markdown类型
    // const isMd = categories.includes('[Markdown]');

    // 文件名
    const mdname = this.replaceFileName(String.raw`${postObj.title}_[${postObj.postid}]_.md`);


    // 替换文件内容
    let content = '';
    // 分类为空
    if (categories.length === 0) {
      // 替换文件内容
      content = this.originToLocal(postObj.description, postObj.postid, isMd, '.');
      // 文件路径
      const mdpath = path.join(abstractPath, mdname);
      fs.writeFileSync(mdpath, content);
    } else {
      // 替换文件内容
      content = this.originToLocal(postObj.description, postObj.postid, isMd);
      // 有分类
      categories.forEach((element) => {
        // 具体到分类的文件夹
        const categoriePath = path.join(abstractPath, element);
        if (fs.existsSync(categoriePath) === false) {
          this.mkdirs(categoriePath);
        }
        // 文件路径
        const mdpath = path.join(categoriePath, mdname);
        fs.writeFileSync(mdpath, content);
      });
    }
    // 发布消息
    mc.emit('DownloadMd.postTolocal.end', { title: postObj.title, postId: postObj.postid });
    return true;
  }

  /**
   * 将获取到的博客园md文件内容转换成本地
   * @param {*} fileContent md文件内容
   * @param {*} postid 帖子id
   * @returns 转换完成的文件内容
   */
  originToLocal(fileContent = '', postid, isMd = true, abstractPathPrefix = '..') {
    let result = fileContent;
    // 处理非markdown类型的富文本类型
    if (isMd === false) {
      result = html2md(String.raw`${result}`);
    }
    // 下载图片
    const downImgDescribe = this.downNetworkImages(md.getNetWorkImage(result), `${postid}`, abstractPathPrefix);
    for (const key in downImgDescribe) {
      // 遍历
      if (Object.hasOwnProperty.call(downImgDescribe, key)) {
        const item = downImgDescribe[key];
        // 排除下载失败的
        if (item.status === false) {
          continue;
        }
        // key 为原始链接
        result = result.replace(String.raw`${key}`, String.raw`${item.abstractPath}`);
      }
    }
    // 根据下载情况替换文件内容
    return result;
  }

  /**
   * 将网络图片下载到本地
   * @param {*} files 所有网络图片路径
   * @param {*} postid 帖子id
   * @returns 载队列对象
   * * {
     *    网络文件url : {
     *      status : true/false //下载成功或者失败
     *      localPath ： ''  // 本地绝对路径
     *      abstractPath : '' //相对于md文件的相对路径
     *    }
     * }
   */
  downNetworkImages(files = [], postid, abstractPathPrefix = '..') {
    /**
     * fileDownloadQueue 下载队列对象
     * {
     *    网络文件url : {
     *      status : true/false //下载成功或者失败
     *      localPath ： ''  // 本地绝对路径
     *      abstractPath : '' //相对于md文件的相对路径
     *    }
     * }
    */
    const fileDownloadQueue = {};

    // 直接结束
    if (config.downImageFlag === false) {
      for (let index = 0; index < files.length; index++) {
        // 网络图片的链接
        const element = files[index];
        const obj = {
          status: false,
        };
        fileDownloadQueue[element] = obj;
      }
      return fileDownloadQueue;
    }

    // 发布消息
    mc.emit('DownloadMd.downNetworkImages.filesCount', { length: files.length, postId: postid });
    // 图片数量为零，直接结束
    if (files.length === 0) {
      return;
    }
    // 目标md文件的图片路径 ：当前路径 + 博客名 + assets +博客id
    const assetsFolder = path.join(this.descFolder, 'assets', postid);
    if (fs.existsSync(assetsFolder) === false) {
      this.mkdirs(assetsFolder);
    }

    // 下载图片
    for (let index = 0; index < files.length; index++) {
      // 网络图片的链接
      const element = files[index];
      // 发布消息
      mc.emit('DownloadMd.downNetworkImages.nowIndex', { nowIndex: index + 1, postId: postid });
      mc.emit('DownloadMd.downNetworkImages.start', { img: element, postId: postid });
      // 文件名
      const filename = path.parse(element).base;
      // 图片绝对路径
      const localPath = path.join(assetsFolder, filename);
      // 图片相对路径
      const abstractPath = `${abstractPathPrefix}/assets/${postid}/${filename}`;
      // 图片下载是否成功
      let downFlag = true;
      try {
        // 图片下载
        got.stream(element).pipe(fs.createWriteStream(localPath));
      } catch (error) {
        downFlag = false;
      }
      const obj = {
        // status: fs.existsSync(String.raw`${localPath}`),
        status: downFlag,
        localPath,
        abstractPath,
      };
      fileDownloadQueue[element] = obj;
      // 发布消息
      mc.emit('DownloadMd.downNetworkImages.end', { img: element, postId: postid });
    }
    return fileDownloadQueue;
  }


  /**
   * 通过博客链接返回博客名
   * @param {string} link 博客链接
   * @returns 返回博客名
   */
  getBlogNameBylink(link = '') {
    const result = link.replace('https://www.cnblogs.com/', '');
    return result.substring(0, result.indexOf('/'));
  }
}
module.exports = DownloadMd;
